<?php
?>
<html>
	<head>
			<?php include("head2.php"); ?>
		<style>
			<?php include("nav2.css"); ?>
			.head_menu{
				padding: 20px;
			}
		</style>
	</head>
	<body>
		<div class="head_menu">
      <div class="ui top attached tabular menu">
        <a class="item active" data-tab="first">First</a>
        <a class="item" data-tab="second">Second</a>
        <a class="item" data-tab="third">Third</a>
      </div>
      <div class="ui bottom attached tab segment active" data-tab="first">
        First
      </div>
      <div class="ui bottom attached tab segment" data-tab="second">
        Second
      </div>
      <div class="ui bottom attached tab segment" data-tab="third">
        Third
      </div>
      <script>$('.menu .item').tab();</script> 
		</div>
	</body>
</html>